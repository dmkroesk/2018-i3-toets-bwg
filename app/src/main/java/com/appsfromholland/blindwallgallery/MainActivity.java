package com.appsfromholland.blindwallgallery;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MuralListener{

    ArrayList<Mural> murals;

    @Override
    public void onMuralAvailable(Mural mural) {
        murals.add(mural);
        adapter.notifyDataSetChanged();
    }

    ListView listview;
    BWGAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        murals = new ArrayList<>();
        listview = (ListView) findViewById(R.id.muralsListViewId);
        adapter = new BWGAdapter(this, murals);
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), MuralDetailedActivity.class);
                intent.putExtra("MURAL", murals.get(i));
                startActivity(intent);
            }
        });

        // Get all murals
        getMurals();

    }

    private void getMurals() {
        // Clear previous filled arraylist
        murals.clear();

        // Get new Murals
        BWGTask task = new BWGTask(this);
        String url = "https://api.blindwalls.gallery/apiv2/murals";
        String[] urls = new String[] {url};
        task.execute(urls);
    }
}
