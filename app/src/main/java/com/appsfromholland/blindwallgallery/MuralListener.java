package com.appsfromholland.blindwallgallery;

/**
 * Created by dkroeske on 19/02/2018.
 */

public interface MuralListener {
    public void onMuralAvailable(Mural mural);
}
