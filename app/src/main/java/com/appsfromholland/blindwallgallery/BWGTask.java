package com.appsfromholland.blindwallgallery;

/**
 * Created by dkroeske on 19/02/2018.
 */

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Random;

public class BWGTask extends AsyncTask<String, Void, String> {

    private MuralListener listener;

    public BWGTask(MuralListener listener) {
        this.listener = listener;
    }

    @Override
    protected String doInBackground(String... params) {

        BufferedReader bufferedReader = null;
        String response = "";

        try {
            URL url = new URL(params[0]);
            URLConnection connection = url.openConnection();

            bufferedReader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream())
            );
            response = bufferedReader.readLine().toString();
            String line;
            while( (line = bufferedReader.readLine()) != null) {
                response += line;
            }

        } catch (Exception e) {
            return null;
        } finally {
            if( bufferedReader != null ) {
                try {
                    bufferedReader.close();
                } catch (Exception e) {
                    return null;
                }
            }
        }
        return response;
    }

    protected void onPostExecute(String response) {
        Log.d("",response);

        if( response == null )
            return;

        try {

            JSONArray jsonArray = new JSONArray(response);
            for( int idx = 0; idx < jsonArray.length(); idx++) {
                String author = jsonArray.getJSONObject(idx).getString("author");
                String desc = jsonArray.getJSONObject(idx).getJSONObject("description").getString("nl");
                JSONArray images = jsonArray.getJSONObject(idx).getJSONArray("images");
                int index = new Random().nextInt(images.length());
                String imageUrl = "https://api.blindwalls.gallery/" + images.getJSONObject(index).getString("url");

                Mural item = new Mural(author, desc, imageUrl);

                this.listener.onMuralAvailable(item);

                Log.d("","");
            }

            Log.d("","");

        } catch(Exception e) {
            Log.e("","");
        }
    }
}