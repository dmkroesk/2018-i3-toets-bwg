package com.appsfromholland.blindwallgallery;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class MuralDetailedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mural_detailed);

        // Imageview
        ImageView iv = (ImageView) findViewById(R.id.muralImageView);

        // Bol
        Mural mural = (Mural) getIntent().getSerializableExtra("MURAL");

        // Plaatsen van image via picasso
        Picasso.with(this).load(mural.getImageUrl()).into(iv);
    }
}
