package com.appsfromholland.blindwallgallery;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by dkroeske on 19/02/2018.
 */

public class BWGAdapter extends ArrayAdapter<Mural> {

    public BWGAdapter(Context context, ArrayList<Mural> items) {
        super( context, 0, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        // Furby ophalen
        Mural mural = getItem(position);

        // View aanmaken of herbruiken
        if( convertView == null ) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.mural_listview_item,
                    parent,
                    false
            );
        }

        // Koppelen datasource aan UI
        TextView author = (TextView) convertView.findViewById(R.id.lvAuthorId);
        ImageView thumbnail = (ImageView) convertView.findViewById(R.id.lvImageId);

        author.setText(mural.getAuthor());
        Picasso.with(getContext()).load(mural.getImageUrl()).into(thumbnail);

        return convertView;
    }
}
